export default class SwapiService {
    _apiBase = "https://swapi.dev/api"

    async getResources(url) {
        const resp = await fetch(`${this._apiBase}${url}`);
        if (!resp.ok) {
            throw new Error(`could not fetch ${url}, received ${resp.status}, error: ${resp.error}`)
        }
        const body = await resp.json();
        return body;
    };

    async getAllPeople() {
        const res = await this.getResources(`/people/`);
        return res.results.map(this._transformPlanet);
    }

    async getPerson(id) {
        const res = await this.getResources(`/people/${id}/`)
        return this._transformPerson(res)
    }

    async getAllPlanets() {
        const res = await this.getResources(`/planets/`);
        return res.results.map(this._transformPlanet);
    }

    async getPlanet(id) {
        const planet = await this.getResources(`/planets/${id}/`)
        return this._transformPlanet(planet);
    }

    async getAllStarships() {
        const res = await this.getResources(`/starships/`);
        return res.results.map(this._transformStarship());
    }

    async getStarship(id) {
        const res = await this.getResources(`/starships/${id}/`)
        return this._transformStarship(res)
    }

    _extractId(item) {
        const idRegExp = /\/([0-9]*)\/$/;
        return item.url.match(idRegExp)[1];
    }

    _transformPlanet(p) {
        return {
            id: this._extractId(p),
            name: p.name,
            population: p.population,
            rotationPeriod: p.rotation_period,
            diameter: p.diameter,
        };
    }

    _transformStarship(starship) {
        return {
            id: this._extractId(starship),
            name: starship.name,
            model: starship.model,
            manufacturer: starship.manufacturer,
            costInCredits: starship.costInCredits,
            length: starship.length,
            crew: starship.crew,
            passengers: starship.passengers,
            cargoCapacity: starship.cargoCapacity
        }
    }

    _transformPerson(person) {
        return {
            id: this._extractId(person),
            name: person.name,
            gender: person.gender,
            birthYear: person.birthYear,
            eyeColor: person.eyeColor
        }
    }
}
