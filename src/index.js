import React from 'react';
import ReactDOM from 'react-dom';
import App from "./components/app";

ReactDOM.render(<App />,
    document.getElementById('root'));
// const swapi = new SwapiService();
// swapi.getAllPeople().then((people) => {
//     people.forEach((p) => {
//         console.log(p.name);
//     })
// });
//
// swapi.getPerson(3).then((p) => {
//     console.log("person")
//     console.log(p.name);
// });


// fetch("https://swapi.dev/api/people/1/").
//     then((resp) => {
//         return resp.json();
// }).then((body)=> {
//     console.log(body);
// })

// эквивалентен этому:

// const getResources = async (url) => {
//     const resp = await fetch(url);
//     if (!resp.ok) {
//         throw new Error(`could not fetch ${url}, received ${resp.status}, error: ${resp.error}`)
//     }
//     const body = await resp.json();
//     return body;
// };
//
// getResources("https://swapi.dev/api/people/1asdasdas/").then((body) => {
//     console.log(body)
// }).catch((err) => {
//     console.error("could not fetch:", err);
// })
//
